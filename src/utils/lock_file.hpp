#pragma once
#include <string>

namespace utils::filesystem {
class LockFile {
 public:
  LockFile(std::string path) : path_(std::move(path) + ".lock") {}

  void Lock();
  void lock() { Lock(); }

  void Unlock();
  void unlock() { Unlock(); }

 private:
  std::string path_;
  int fd_ = -1;
};
}  // namespace utils::filesystem
