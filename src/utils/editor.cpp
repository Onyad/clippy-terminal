#include <utils/editor.hpp>
#include <utils/config_path.hpp>

#include <cppshell/shell.hpp>

namespace utils {
void OpenEditor(const std::string& file) {
  std::string editors[] = {"nvim", "vim", "vi"};

  auto tmp_path = utils::GetProjectDirectory() / "tmp";
  std::filesystem::create_directories(tmp_path);

  cppshell::Shell shell("bash", tmp_path);

  for (auto& editor : editors) {
    shell.Execute(editor + " " + file);
    if (shell.GetExitCodeLastCommand() == 0) {
      break;
    }
  }
}
}  // namespace utils
