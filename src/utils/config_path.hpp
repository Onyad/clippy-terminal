#pragma once

#include <string>
#include <vector>
#include <ranges>
#include <filesystem>

namespace utils {
inline std::filesystem::path GetProjectDirectory() {
  namespace fs = std::filesystem;
  using std::filesystem::path;

  path config_path =
      std::string(std::getenv("HOME")) + "/.local/share/clippy-terminal/";

  fs::create_directories(config_path);

  return config_path;
}
}  // namespace utils
