#pragma once

#include <random>
#include <chrono>

namespace utils::random {
static inline std::mt19937_64 rnd(std::chrono::system_clock::now().time_since_epoch().count());

inline uint64_t RandInt(uint64_t max_n) {
  return rnd() % max_n;
}
}
