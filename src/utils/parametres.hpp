#pragma once
#include <vector>
#include <string>
#include <stdexcept>

namespace utils::parametres {
enum class Parameter { Skip, Nothing, Anything };

namespace detail {
template <size_t index, typename... Args>
bool CheckPatternParametresImpl(const std::vector<std::string>& args,
                                Parameter t) {
  if (t == Parameter::Nothing) {
    return index == args.size();
  } else if (t == Parameter::Anything) {
    return true;
  }
  return false;
}

template <size_t index, typename T, typename... Args,
          std::enable_if_t<!std::is_same_v<T, Parameter>, bool> = true>
bool CheckPatternParametresImpl(const std::vector<std::string>& args, T&& head,
                                Args&&... pattern);

template <size_t index, typename T, typename... Args,
          std::enable_if_t<std::is_same_v<T, Parameter>, bool> = true>
bool CheckPatternParametresImpl(const std::vector<std::string>& args, T&& p,
                                Args&&... pattern) {
  if (index >= args.size()) {
    return false;
  }

  if (p != Parameter::Skip) {
    throw std::logic_error("Unsupported parameter");
  }

  return CheckPatternParametresImpl<index + 1, Args...>(
      args, std::forward<Args>(pattern)...);
}

template <size_t index, typename T, typename... Args,
          std::enable_if_t<!std::is_same_v<T, Parameter>, bool>>
bool CheckPatternParametresImpl(const std::vector<std::string>& args, T&& head,
                                Args&&... pattern) {
  if (index >= args.size()) {
    return false;
  }
  if (args[index] != head) {
    return false;
  }
  return CheckPatternParametresImpl<index + 1, Args...>(
      args, std::forward<Args>(pattern)...);
}
}  // namespace detail

std::vector<std::string> ParseInputParameters(int argc, char* argv[]);

template <typename... Args>
bool CheckPatternParametres(const std::vector<std::string>& args,
                            Args&&... pattern) {
  return detail::CheckPatternParametresImpl<0, Args...>(
      args, std::forward<Args>(pattern)...);
}

}  // namespace utils
