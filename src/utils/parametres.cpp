#include <utils/parametres.hpp>

namespace utils::parametres {
std::vector<std::string> ParseInputParameters(int argc, char* argv[]) {
  std::vector<std::string> result;
  result.reserve(argc);

  for (size_t i = 0; i < argc; ++i) {
    result.emplace_back(argv[i]);
  }

  return result;
}
}  // namespace utils
