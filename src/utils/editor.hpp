#pragma once

#include <string>

namespace utils {
void OpenEditor(const std::string& file);
}
