#pragma once

#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <random>
#include <chrono>

#include <utils/random.hpp>

namespace utils::filesystem {
inline std::string LoadFile(const std::filesystem::path& file) {
  std::ifstream in(file);

  std::string result;
  std::string tmp;

  while (std::getline(in, tmp)) {
    result += tmp;
    result += "\n";
  }

  return result;
}

inline std::filesystem::path GenerateFile(std::filesystem::path dir) {
  if (!std::filesystem::is_directory(dir)) {
    throw std::logic_error(std::string(dir) + " is not directory");
  }

  static auto RandomSymbol = []() {
    int n = utils::random::RandInt(26 + 26 + 10);
    if (n < 26) {
      return 'a' + n;
    } else if (n < 26 + 26) {
      return 'A' + n - 26;
    } else {
      return '0' + n - 26 - 26;
    }
  };

  auto GenerateFilename = [length = 6]() mutable {
    if (length > 10) {
      throw std::logic_error("failed to create file");
    }
    std::string filename;
    for (size_t i = 0; i < length; ++i) {
      filename += RandomSymbol();
    }
    length++;

    return filename;
  };

  auto filename = GenerateFilename();
  while (true) {
    if (std::filesystem::status(dir / filename).type() == std::filesystem::file_type::not_found) {
      break;
    }

    filename = GenerateFilename();
  }

  return dir / filename;
}
}  // namespace utils::filesystem
