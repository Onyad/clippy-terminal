#include <utils/lock_file.hpp>

#include <unistd.h>
#include <sys/file.h>

namespace utils::filesystem {
void LockFile::Lock() {
  fd_ = open(path_.data(), O_RDWR | O_CREAT, 0666);
  while (flock(fd_, LOCK_EX) != 0) {
  }
}

void LockFile::Unlock() {
  while (flock(fd_, LOCK_UN) != 0) {
  }
  close(fd_);
}
}  // namespace utils::filesystem
