#pragma once
#include <clippy/config.hpp>

#include <mutex>
#include <stdexcept>
#include <utils/lock_file.hpp>

#include <vector>
#include <filesystem>
#include <optional>

#include <jsoncons/json.hpp>
#include "utils/config_path.hpp"
#include "utils/editor.hpp"
#include "utils/filesystem.hpp"

struct Project {
  Project() {}

  Project(std::filesystem::path root_project, std::filesystem::path configuration_file)
      : root_project(root_project), configuration_file(configuration_file) {}

  std::strong_ordering operator<=>(const Project&) const = default;

  Config GetConfig() {
    return Config{configuration_file, root_project};
  }

  std::filesystem::path root_project;
  std::filesystem::path configuration_file;
  std::optional<std::string> name;
  std::optional<std::filesystem::path> open_script;
};

class ProjectList {
 public:
  ProjectList(std::filesystem::path path) : path_(std::move(path)), lock_file_(path_) {
    Load();
  }

  Config GetNewConfig(const std::filesystem::path& config_directory);

  const std::vector<Project>& GetProjects() const {
    return projects_;
  }

  std::optional<Project> GetCurrentProject() {
    auto* p = GetCurrentProject_();
    if (p) {
      return *p;
    } else {
      return std::nullopt;
    }
  }

  void SetNameCurrentProject(const std::string& name) {
    auto* p = GetCurrentProject_();
    if (p) {
      p->name = name;
      SaveConfig();
    } else {
      throw std::logic_error("Not exists current project");
    }
  }

  std::filesystem::path GetCurrentLoadConfig() {
    auto* p = GetCurrentProject_();
    if (p) {
      if (p->open_script) {
        return p->open_script.value();
      } else {
        auto scripts_path = utils::GetProjectDirectory() / "scripts";
        {
          std::lock_guard lock(lock_file_);
          p->open_script = utils::filesystem::GenerateFile(scripts_path);
          SaveConfig();
        }
        return p->open_script.value();
      }
    } else {
      throw std::logic_error("Not exists current project");
    }
  }

  std::optional<Project> GetProjectByName(const std::string& name) {
    auto* p = GetProjectByName_(name);
    if (p) {
      return *p;
    } else {
      return std::nullopt;
    }
  }

 private:
  Project* GetCurrentProject_();
  Project* GetProjectByName_(const std::string&);

  void OldLoadConfig(const std::string&);
  void SaveConfig();

  void Load();
  void LoadConfig(const jsoncons::json&);
  void LoadWithoutLock();

 private:
  std::filesystem::path path_;
  utils::filesystem::LockFile lock_file_;

  std::vector<Project> projects_;
};
