#include <clippy/config.hpp>
#include <clippy/target.hpp>

#include <fstream>
#include <algorithm>

std::string Strip(std::string s) {
  while (!s.empty() && std::isspace(s.back())) {
    s.pop_back();
  }
  std::reverse(s.begin(), s.end());
  while (!s.empty() && std::isspace(s.back())) {
    s.pop_back();
  }
  std::reverse(s.begin(), s.end());

  return s;
}

std::unique_ptr<clippy::targets::RunShellScript> Config::GetTarget(
    const std::string& target) {
  std::ifstream in(path_);

  std::string current;
  std::vector<std::string> target_commands;
  target_commands.emplace_back("cd " + initial_directory_);

  bool target_exists = false;
  bool in_target = false;

  while (std::getline(in, current)) {
    if (current == target + ":") {
      target_exists = true;
      in_target = true;
      continue;
    }

    if (current.empty()) {
      continue;
    }

    if (!std::isspace(current[0])) {
      in_target = false;
    }

    if (in_target) {
      target_commands.emplace_back(Strip(std::move(current)));
    } else if (current[0] == '!') {
      target_commands.emplace_back(current.substr(1, current.size() - 1));
    }
  }

  if (!target_exists) {
    return nullptr;
  }

  return std::make_unique<clippy::targets::RunShellScript>(target_commands);
}
