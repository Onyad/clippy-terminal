#include <clippy/project_list.hpp>
#include <filesystem>
#include <optional>
#include <sstream>
#include <utils/lock_file.hpp>
#include <utils/filesystem.hpp>

#include <fstream>
#include <mutex>
#include <random>
#include <chrono>

#include <utils/filesystem.hpp>

#include <jsoncons/json.hpp>
#include <jsoncons_ext/cbor/cbor.hpp>

Config ProjectList::GetNewConfig(const std::filesystem::path& config_directory) {
  std::lock_guard guard(lock_file_);

  LoadWithoutLock();

  auto path_to_config = utils::filesystem::GenerateFile(config_directory);

  projects_.emplace_back(std::filesystem::current_path(), path_to_config);

  SaveConfig();

  return {path_to_config, std::filesystem::current_path()};
}

void ProjectList::Load() {
  std::lock_guard guard(lock_file_);

  LoadWithoutLock();
}

void ProjectList::OldLoadConfig(const std::string& data) {
  std::stringstream in(data);

  Project current;

  while (in >> current.root_project >> current.configuration_file) {
    projects_.push_back(current);
  }
}

template <typename U, typename T>
void UpdateField(jsoncons::json& data, const std::string& field, std::optional<T> member) {
  if (member) {
    data[field] = static_cast<U>(member.value());
  }
}

void ProjectList::SaveConfig() {
  jsoncons::json result;

  result["version"] = "0.1";
  result["projects"] = std::vector<std::string>();

  for (auto& project : projects_) {
    jsoncons::json current;
    current["path_to_config"] = std::string(project.configuration_file);
    current["path_root_project"] = std::string(project.root_project);

    UpdateField<std::string>(current, "name", project.name);
    UpdateField<std::string>(current, "open_script", project.open_script);

    result["projects"].emplace_back(current);
  }

  std::ofstream out(path_);
  out << result.to_string();
}

template <typename T>
std::optional<T> GetOptionalField(const jsoncons::json& data, std::string field) {
  return data.contains(field) ? std::make_optional(data[field].as<std::string>()) : std::nullopt;
}

void ProjectList::LoadConfig(const jsoncons::json& data) {
  if (data["version"] != "0.1") {
    throw std::logic_error("unsupported version config");
  }

  Project current;
  for (auto& project : data["projects"].array_range()) {
    current.configuration_file = project["path_to_config"].as<std::string>();
    current.root_project = project["path_root_project"].as<std::string>();

    current.name = GetOptionalField<std::string>(project, "name");
    current.open_script = GetOptionalField<std::string>(project, "open_script");

    projects_.emplace_back(current);
  }
}

void ProjectList::LoadWithoutLock() {
  projects_.clear();
  auto data = utils::filesystem::LoadFile(path_);

  try {
    LoadConfig(jsoncons::json::parse(data));
  } catch (...) {
    std::cout << "I can't read project lists. Try fix it?";

    std::string result;
    std::getline(std::cin, result);

    if (result == "y") {
      OldLoadConfig(data);
      SaveConfig();
    }
  }
}

Project* ProjectList::GetCurrentProject_() {
  auto current_path = std::filesystem::current_path();

  Project* result = nullptr;
  auto UpdateResult = [&result](Project& p) {
    if (!result) {
      result = &p;
    } else {
      if (*result > p) {
        result = &p;
      }
    }
  };

  for (auto& project : projects_) {
    auto prefix_current_path = current_path;

    if (project.root_project == "/") {
      UpdateResult(project);
      continue;
    }

    while (prefix_current_path != "/") {
      if (prefix_current_path == project.root_project) {
        UpdateResult(project);
      }
      prefix_current_path = prefix_current_path.parent_path();
    }
  }

  return result;
}

Project* ProjectList::GetProjectByName_(const std::string& name) {
  Project* result = nullptr;
  for (auto& project : projects_) {
    if (!project.name) {
      continue;
    }
    if (project.name == name) {
      result = &project;
    }
  }
  return result;
}
