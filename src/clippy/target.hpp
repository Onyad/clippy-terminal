#pragma once

#include <clippy/project_list.hpp>
#include <clippy/config.hpp>

#include <utils/editor.hpp>
#include <utils/config_path.hpp>

#include <cppshell/shell.hpp>

#include <rang.hpp>

#include <iostream>
#include <deque>
#include <ranges>
#include <functional>
#include "utils/filesystem.hpp"

#include <tmuxub/tmuxub.hpp>

namespace clippy::targets {
class Target {
 public:
  virtual void Execute() = 0;
  virtual ~Target() = default;
};

class EmptyTarget : public Target {
 public:
  void Execute() override {}
  ~EmptyTarget() override {}
};

class OpenProjectConfig : public Target {
 public:
  OpenProjectConfig(Config config) : config_(config) {}

  void Execute() override {
    config_.Edit();
  }
  ~OpenProjectConfig() override {}

 private:
  Config config_;
};

class CreateProjectConfig : public Target {
 public:
  CreateProjectConfig(ProjectList& projects) : projects_(projects) {}

  void Execute() override {
    auto scripts_path = utils::GetProjectDirectory() / "scripts";
    std::filesystem::create_directories(scripts_path);

    auto config = projects_.GetNewConfig(scripts_path);
    config.Edit();
  }
  ~CreateProjectConfig() override {}

 private:
  ProjectList& projects_;
};

class RunShellScript : public Target {
 public:
  template <template <typename, typename...> class C, typename... Params>
  RunShellScript(const C<std::string, Params...>& commands)
      : commands_(commands.begin(), commands.end()) {}

  void PushBack(const std::string& command) {
    commands_.push_back(command);
  }

  void PushFront(const std::string& command) {
    commands_.push_front(command);
  }

  void Execute() override {
    auto tmp_path = utils::GetProjectDirectory() / "tmp";
    std::filesystem::create_directories(tmp_path);

    cppshell::Shell s("bash", tmp_path);

    for (auto& command : commands_) {
      std::cout << rang::fg::green << rang::style::bold << rang::bgB::blue << "->"
                << rang::fg::reset << rang::bg::reset << " " << command << rang::style::reset
                << std::endl;
      s.Execute(command);
      if (int err = s.GetExitCodeLastCommand(); err != 0) {
        std::cout << rang::fg::red << rang::style::bold << "Command exit with code " << err
                  << rang::fg::reset << rang::style::reset << std::endl;
        std::cout << rang::fg::blue << rang::style::bold << "Continue execution? [Y/n] "
                  << rang::fg::reset << rang::style::reset;

        std::string result;
        std::getline(std::cin, result);
        if (result == "" || result == "y") {
          continue;
        } else {
          return;
        }
      }
    }
  }

  ~RunShellScript() override {}

 private:
  std::deque<std::string> commands_;
};

class PrintListProjects : public Target {
 public:
  PrintListProjects(ProjectList& projects) : projects_(projects) {}

  void Execute() override {
    for (auto& project : projects_.GetProjects()) {
      std::cout << std::setw(10) << project.name.value_or("-") << " " << std::setw(50)
                << project.root_project << " " << std::setw(70) << project.configuration_file
                << std::setw(70) << project.open_script.value_or("-") << std::endl;
    }
  }

 private:
  ProjectList& projects_;
};

class SetNameProject : public Target {
 public:
  SetNameProject(ProjectList& projects, std::string new_name)
      : projects_(projects), new_name_(std::move(new_name)) {}

  void Execute() override {
    projects_.SetNameCurrentProject(new_name_);
  }

 private:
  ProjectList& projects_;
  std::string new_name_;
};

class OpenLoadConfig : public Target {
 public:
  OpenLoadConfig(ProjectList& projects) : projects_(projects) {}

  void Execute() override {
    utils::OpenEditor(projects_.GetCurrentLoadConfig());
  }

 private:
  ProjectList& projects_;
  std::string new_name_;
};

class OpenProject : public Target {
 public:
  OpenProject(ProjectList& projects, const std::string& name) : projects_(projects), name_(name) {}

  void Execute() override {
    auto p = projects_.GetProjectByName(name_);

    tmuxub::Tmux t(utils::GetProjectDirectory() / "tmuxsocket");
    if (t.HasSession(name_)) {
      t.ConnectToSession(name_);
      t.Exec();
    } else {
      auto file = utils::filesystem::LoadFile(p->open_script.value());
      chdir(std::string(p.value().root_project).data());
      t.CreateSession(name_);
      t.SplitWindow(file);
      t.Exec();
    }
  }

 private:
  ProjectList& projects_;
  std::string name_;
};
}  // namespace clippy::targets
