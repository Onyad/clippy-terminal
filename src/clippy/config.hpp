#pragma once

#include <utils/editor.hpp>

#include <string>
#include <memory>

namespace clippy::targets {
class Target;
class RunShellScript;
}

class Config {
 public:
  Config(std::string path, std::string initial_directory)
      : path_(std::move(path)), initial_directory_(initial_directory) {}

  void Edit() { utils::OpenEditor(path_); }

  std::unique_ptr<clippy::targets::RunShellScript> GetTarget(const std::string& target);

 private:
  std::string path_;
  std::string initial_directory_;
};
