#include <clippy/clippy.hpp>
#include <clippy/target.hpp>

#include <utils/parametres.hpp>
#include <utils/config_path.hpp>

#include <iostream>
#include <memory>
#include <vector>
#include <filesystem>

#include <rang.hpp>

void Clippy::Run(const std::vector<std::string>& args) {
  if (auto result = TryExecuteClippyCommand(args); result) {
    result->Execute();
    return;
  }
  if (auto result = GetScriptTarget(args); result) {
    result->Execute();
    return;
  }

  std::cout << rang::bg::red << rang::style::bold << "Unsupported parameters { ";
  for (size_t i = 1; i < args.size(); ++i) {
    std::cout << args[i] << (i + 1 == args.size() ? "" : ",") << " ";
  }
  std::cout << "}" << rang::bg::reset << rang::style::reset << std::endl;
}

Clippy::TargetPtr Clippy::TryExecuteClippyCommand(const std::vector<std::string>& args) {
  using namespace utils::parametres;
  using namespace clippy::targets;
  if (CheckPatternParametres(args, Parameter::Skip, "help", Parameter::Anything)) {
    std::cout << "Hello I'm clippy" << std::endl;
    std::cout << "Parametres: { ";
    for (size_t i = 0; i < args.size(); ++i) {
      std::cout << args[i] << (i + 1 == args.size() ? "" : ",") << " ";
    }
    std::cout << "}" << std::endl;

    std::cout << "You can use:\n  cfg\n  crt\n  prj\n  list\n  setname\n  loadcfg\n  op" << std::endl;

    return std::make_unique<EmptyTarget>();
  }

  if (CheckPatternParametres(args, Parameter::Skip, "cfg", Parameter::Anything)) {
    LoadProjects();
    auto p = projects_->GetCurrentProject();

    if (p.has_value()) {
      return std::make_unique<OpenProjectConfig>(p->GetConfig());
    } else {
      return std::make_unique<CreateProjectConfig>(projects_.value());
    }
  }

  if (CheckPatternParametres(args, Parameter::Skip, "crt", Parameter::Anything)) {
    LoadProjects();
    return std::make_unique<CreateProjectConfig>(projects_.value());
  }

  if (CheckPatternParametres(args, Parameter::Skip, "prj", Parameter::Anything)) {
    // TODO description current project or project by name
    return nullptr;
  }

  if (CheckPatternParametres(args, Parameter::Skip, "list", Parameter::Nothing)) {
    LoadProjects();

    return std::make_unique<PrintListProjects>(projects_.value());
  }

  if (CheckPatternParametres(args, Parameter::Skip, "setname", Parameter::Anything)) {
    LoadProjects();

    return std::make_unique<SetNameProject>(projects_.value(), args[2]);
  }

  if (CheckPatternParametres(args, Parameter::Skip, "loadcfg", Parameter::Nothing)) {
    LoadProjects();

    return std::make_unique<OpenLoadConfig>(projects_.value());
  }

  if (CheckPatternParametres(args, Parameter::Skip, "op", Parameter::Anything)) {
    LoadProjects();

    return std::make_unique<OpenProject>(projects_.value(), args[2]);
  }

  return nullptr;
}

Clippy::TargetPtr Clippy::GetScriptTarget(const std::vector<std::string>& args) {
  LoadProjects();
  auto p = projects_->GetCurrentProject();

  if (!p.has_value() || args.size() < 2) {
    return nullptr;
  }

  auto cfg = p->GetConfig();
  auto result = cfg.GetTarget(args[1]);

  if (result && enable_aliases_) {
    result->PushFront("shopt -s expand_aliases");
  }

  return result;
}

void Clippy::LoadProjects() {
  if (!projects_.has_value()) {
    projects_.emplace(utils::GetProjectDirectory() / "projects");
  }
}
