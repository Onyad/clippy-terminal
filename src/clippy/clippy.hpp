#pragma once

#include <clippy/project_list.hpp>
#include <clippy/target.hpp>

#include <vector>
#include <string>
#include <memory>

class Clippy {
 public:
  Clippy(bool enable_aliases = true) : enable_aliases_(enable_aliases) {}

  void Run(const std::vector<std::string>& args);

  using TargetPtr = std::unique_ptr<clippy::targets::Target>;
 private:
  TargetPtr TryExecuteClippyCommand(const std::vector<std::string>& args);
  TargetPtr GetScriptTarget(const std::vector<std::string>& args);

  void LoadProjects();

  bool enable_aliases_;
  std::optional<ProjectList> projects_;
};
