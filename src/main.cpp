#include <clippy/clippy.hpp>
#include <utils/parametres.hpp>

#include <vector>
#include <string>

#include <csignal>

#include <utils/filesystem.hpp>

#include <jsoncons/json.hpp>

int main(int argc, char* argv[]) {
  std::signal(SIGPIPE, SIG_IGN);

  using namespace utils::parametres;
  auto params = ParseInputParameters(argc, argv);

  Clippy clippy;
  clippy.Run(params);
}
